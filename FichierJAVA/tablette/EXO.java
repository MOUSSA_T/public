

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

//import javax.imageio.ImageIO;
public class EXO extends JFrame{
	private static final long serialVersionUID = 1L;
	
	
	int valeurSlider;
	DrawPanel dp;
	JButton beff,benr,bgomm,noire,rouge,vert,gris,jaune,orange;
	JPanel pan,n,r,v,g,j,o;
	GridLayout gd;
	JToolBar toolbar;
	JMenuItem itn,itr,itv,itg,itj,ito,ouvrir,effec,enreg,quiter,gommer,voir,quiterchoix,choisir,couleur;
	JMenu menu,sousmenu,menuinc;
	JMenuBar menubar;
	JSlider slider;
	JTextField fichier;
	JFileChooser choixfichier;
	String nomfichier;
	protected int valg=0;
	private boolean fermer= false,optquiter=false;
	
	public EXO(){
		menubar = new JMenuBar();
		sousmenu = new JMenu("Couleur");
		
		itn= new JMenuItem("noire");
		itr= new JMenuItem("rouge");
		itv= new JMenuItem("vert");
		itg= new JMenuItem("gris");
		itj= new JMenuItem("jaune");
		ito= new JMenuItem("orange");
		ouvrir= new JMenuItem("ouvrir"); 
		effec = new JMenuItem("effacer");
		gommer = new JMenuItem("gommer");
		enreg= new JMenuItem("enregistrer");
		quiter= new JMenuItem("quiter");
		couleur= new JMenuItem("Couleur");
		menu = new JMenu("Fichier");
		menuinc= new JMenu("Incerer");
		beff =  new JButton("Effacer");
		benr = new JButton("Enregistrer");
		bgomm = new JButton("Gommer");
		choisir = new JMenuItem("enregistrer sous...");
		dp = new DrawPanel(); 
		n = new JPanel();
		r = new JPanel();
		v = new JPanel();
		g = new JPanel();
		j = new JPanel();
		o = new JPanel();
		
		
		toolbar = new JToolBar();
		gd = new GridLayout(1,3);
		noire = new JButton();
		rouge = new JButton();
		vert = new JButton();
		gris =new JButton();
		jaune= new JButton();
		orange= new JButton();
		slider= new JSlider();
		choixfichier = new JFileChooser(".");
		
		
		n.setBackground(Color.BLACK);
		r.setBackground(Color.RED);
		v.setBackground(Color.GREEN);
		g.setBackground(Color.GRAY);
		j.setBackground(Color.YELLOW);
		o.setBackground(Color.ORANGE);
		noire.add(n);
		rouge.add(r);
		vert.add(v);
		gris.add(g);
		jaune.add(j);
		orange.add(o);

		/********pour introduire le slider*********/
		slider.setMaximum(10);
		slider.setMinimum(0);
		slider.setValue(0);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMinorTickSpacing(1);
		slider.setMajorTickSpacing(2);
		
		
		this.toolbar.add(noire);
		this.toolbar.add(rouge);
		this.toolbar.add(vert);
		this.toolbar.add(gris);
		this.toolbar.add(jaune);
		this.toolbar.add(orange);
		this.toolbar.add(slider);
		this.add(toolbar);
		
		
		sousmenu.add(itn);
		sousmenu.add(itr);
		sousmenu.add(itv);
		sousmenu.add(itg);
		sousmenu.add(itj);
		sousmenu.add(ito);
		
		menu.add(ouvrir);
		menu.add(enreg);
		menu.add(choisir);
		menu.add(effec);
		menu.add(gommer);
		menu.add(sousmenu);
		menu.add(quiter);
		menuinc.add(couleur);
		menubar.add(menu);
		menubar.add(menuinc);
		this.setJMenuBar(menubar);
		
		pan = new JPanel(gd);
		pan.add(beff);
		pan.add(bgomm);
		pan.add(benr);

		
		this.add(toolbar,BorderLayout.NORTH);
		beff.addActionListener(new AppuitButton());
		benr.addActionListener(new AppuitButton());
		bgomm.addActionListener(new AppuitButton());
		effec.addActionListener(new AppuitButton());
		quiter.addActionListener(new AppuitButton());
		enreg.addActionListener(new AppuitButton());
		choisir.addActionListener(new Choixfichier());
		ouvrir.addActionListener(new  Choixfichier());
		noire.addActionListener(new ChangeCouleur());
		vert.addActionListener(new ChangeCouleur());
		rouge.addActionListener(new ChangeCouleur());
		gris.addActionListener(new ChangeCouleur());
		jaune.addActionListener(new ChangeCouleur());
		orange.addActionListener(new ChangeCouleur());
		itn.addActionListener(new ChangeCouleur());
		itr.addActionListener(new ChangeCouleur());
		itv.addActionListener(new ChangeCouleur());
		couleur.addActionListener(new ChangeCouleur());
		slider.addChangeListener(new UtiliserSlider());
		
		this.menu.setMnemonic('F');
		this.menuinc.setMnemonic('I');
		this.ouvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,InputEvent.CTRL_MASK));
		this.quiter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,InputEvent.CTRL_MASK));
		this.effec.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
		this.enreg.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_MASK));
		this.gommer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,InputEvent.CTRL_MASK));
		this.couleur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.CTRL_MASK));
		this.choisir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK));
		
		this.getContentPane().add(pan,BorderLayout.SOUTH);
		this.getContentPane().add(dp,BorderLayout.CENTER);
		this.setSize(300, 300);
		this.setTitle("EXO1");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.pack();
		this.setVisible(true);
	
		
	}
	/*************************/
	
	public void saveGraph(DrawPanel dps,File outFile) {
		
	    BufferedImage outImage = new BufferedImage(dps.getWidth(),dps.getHeight(),BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics = outImage.createGraphics();
	    //dps.paintAll(graphics);
	    dp.paintComponent(graphics);
	    pan.paint(graphics);
	  //  File outFile = new File("./out_image.png");
	    try {
	        if (ImageIO.write(outImage,"png",outFile))
	           System.out.println("-- saved");
	      //  ImageIO.write(outImage, "PNG", new File("c:\\yourImageName.PNG"));
	       // ImageIO.write(outImage, "JPEG", new File("c:\\yourImageName.JPG"));
	       // ImageIO.write(outImage, "gif", new File("c:\\yourImageName.GIF"));
	        //ImageIO.write(outImage, "BMP", new File("c:\\yourImageName.BMP"));
	    } catch (IOException e) {
	        System.out.println("erreur dans l'enregistrement de l'image :");
	        e.printStackTrace();
	    }
	}
	/**************************/
	public void openGraph(File infile){
		 try
		    {
		      // the line that reads the image file
		 
		      if((ImageIO.read(infile))!=null)
		    	  System.out.println("--Opened");
		    } 
		    catch (IOException e)
		    {
		    	System.out.println("erreur dans l'ouverture de l'image :");
		        e.printStackTrace();
		    }
	}
	/***********************/
	/******
	public void ImprimerGraphe(Frame dps){
		// Récupère le travail et affiche la boite de dialogue d'impression 
		PrintJob job = getToolkit().getPrintJob(dps,"essai", null);
		if (job != null) {
		        // Recupere le Graphics dans lequel on va ecrire 
		        Graphics g = job.getGraphics();
		        if (g != null) {
		                // Sur le Container imprime l'ensemble de ses Components  
		                cont.printAll(g);
		                g.dispose();
		        } 
		        // Finit le travail
		        job.end();
		} 
	}
	*/
	/***********************/
	
	//recuperer la valeur de du slider
		public class UtiliserSlider implements ChangeListener{
			public void stateChanged(ChangeEvent e){
				valeurSlider=((JSlider)e.getSource()).getValue();
				dp.setPointSize(valeurSlider);
			}	
		}
			
	/********************/
	
	//pour effacer le dessin qu'on vient de faire
	public class AppuitButton implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource()==beff || e.getSource()==effec){
				
				if(valg!=0){
					try{	
						int option = JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir Effacer ?", " Info ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if(option == JOptionPane.OK_OPTION){
							fermer = true;
						}
					
					}catch(Exception exp){exp.printStackTrace();}
					
					if(fermer==true){
						valg=0;
						fermer=false;
						dp.erase();
					}
			
				}
			}
			if(e.getSource()==benr || e.getSource()==enreg){
				saveGraph(dp,new File("./out_image.png"));
			}
			if(e.getSource()==quiter){

				if(valg!=0){
						try{	
							int option = JOptionPane.showConfirmDialog(null, "Êtes-vous sûr de vouloir quitter ?", " Info ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
							if(option == JOptionPane.OK_OPTION){
								optquiter = true;
							}
						
						}catch(Exception exp){exp.printStackTrace();}
						
						if(optquiter==true){
							optquiter=false;
							valg=0;
							System.exit(0);
				}		
				
				}
				else{
					System.exit(0);
				}
			}
			if(e.getSource()==bgomm){
				dp.setPointerColor(Color.white);
			}
		}	
	}
	
	/*****************************/
		//enregsitrer l'element dans un fichier au choix
	public class Choixfichier implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String [] zs = ImageIO.getReaderFileSuffixes();
			FileNameExtensionFilter zfilter1,zfilter2,zfilter3,zfilter4;
			choixfichier.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			zfilter1 = new FileNameExtensionFilter("jpeg",zs);
			zfilter2 = new FileNameExtensionFilter("png",zs);
			zfilter3 = new FileNameExtensionFilter("gif",zs);
			zfilter4 = new FileNameExtensionFilter("bmp",zs);
			choixfichier.addChoosableFileFilter(zfilter1);
			choixfichier.addChoosableFileFilter(zfilter2);
			choixfichier.addChoosableFileFilter(zfilter3);
			choixfichier.addChoosableFileFilter(zfilter4);
			if(e.getSource() == choisir){
				int returnValueur = choixfichier.showOpenDialog(null);
				if(returnValueur == JFileChooser.APPROVE_OPTION){
					File selectedFile = choixfichier.getSelectedFile();
					System.out.println(selectedFile.getName());
						saveGraph(dp,selectedFile);
				}
			}
			
			if (e.getSource() == ouvrir) {
				int returnVal = choixfichier.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = choixfichier.getSelectedFile();
	            //This is where a real application would open the file.
					System.out.println(file.getName());
					openGraph(file);
				} else {
					System.out.println("Open command cancelled by user." );
				}
			}
		}
	}


		/************************************/
		//ÉCOUTEUR POUR LE CHANGEMENT DE COULEUR
	public class ChangeCouleur implements ActionListener{
					public void actionPerformed(ActionEvent e) {
						
						System.out.println(e.getSource().getClass().getName());
						if(e.getSource().getClass().getName().equals("javax.swing.JMenuItem")){
							
							System.out.println("OK !");
							
							if(e.getSource()==vert || e.getSource()==itv)dp.setPointerColor(Color.green);
							
							else
								if(e.getSource()==rouge || e.getSource()==itr)dp.setPointerColor(Color.red);

							else
								if(e.getSource()==gris || e.getSource()==itg)dp.setPointerColor(Color.gray);

							else
								if(e.getSource()==orange || e.getSource()==itj)dp.setPointerColor(Color.orange);
							
							else
								if(e.getSource()==jaune || e.getSource()==ito)dp.setPointerColor(Color.yellow);
					
							else dp.setPointerColor(Color.black);
						}
						
						else{
							if(e.getSource()==vert || e.getSource()==vert)dp.setPointerColor(Color.green);
					
							else
								if(e.getSource()==rouge || e.getSource()==itr)dp.setPointerColor(Color.red);
							
							else
								if(e.getSource()==gris || e.getSource()==itg)dp.setPointerColor(Color.gray);

							else
								if(e.getSource()==orange || e.getSource()==itj)dp.setPointerColor(Color.orange);
								
							else
								if(e.getSource()==jaune || e.getSource()==ito)dp.setPointerColor(Color.yellow);
					
							else dp.setPointerColor(Color.black);
						}
					
					  if(e.getSource()==couleur){
					 
						Color color = JColorChooser.showDialog(null, "couleur du fond", Color.WHITE);
						if(e.getSource()==couleur )  dp.setPointerColor(color);
					  }
				}
			}
	
/****************************************************************************/
public class Point {
		private int size = 10;
		//Position sur l'axe X : initialisé au dehors du conteneur
		private int x = -10;
		//Position sur l'axe Y : initialisé au dehors du conteneur
		private int y = -10;
		//Constructeur par défaut
		private Color color= Color.black;

		
		public Point(){}
		
		public Point(int x, int y, int size, Color color){
		this.size = size;
		this.x = x;
		this.y = y;
		this.color= color;
		}

	//ACCESSEURS
		
		public int getSize() {
			return size;
		}
		public void setSize(int size) {
			this.size = size;
		}
		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getY() {
			return y;
		}
		public void setY(int y) {
			this.y = y;
		}
		public Color getColor(){
			return color;
		}
		public void setColor(Color color){
			this.color=color;
		}
	}

/**********************************************************/

//CTRL + SHIFT + O pour générer les imports
public class DrawPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	//Pour savoir si on doit dessiner ou non
	private boolean erasing = true;
	private Color pointcolor= Color.black;
	
	//Taille du pointeur
	private int pointerSize=0;
	
	//Collection de points !
	private ArrayList<Point> points = new ArrayList<Point>();
	
	public DrawPanel(){
		this.setPreferredSize(new Dimension(1000,1000));
		this.setOpaque(true);
		this.setBackground(Color.white);
		this.addMouseMotionListener(new Dragueur());
		this.addMouseListener(new Appuyeur());
	
	}
		
	/****************************************************/
	class Appuyeur extends MouseAdapter{
		public void mousePressed(MouseEvent e) {
       //On récupère les coordonnées de la souris et on enlève lamoitié de la taille du pointeur pour centrer le tracé
			points.add(new Point(e.getX() - (pointerSize / 2), e.getY()-(pointerSize / 2), pointerSize, pointcolor));
			valg=1;
			repaint();
		}
		
		public void mouseMoved(MouseEvent e) {}
	}

	/******************************************************/
	class Dragueur extends MouseMotionAdapter{
		public void mouseDragged(MouseEvent e){
			points.add(new Point(e.getX() - (pointerSize / 2), e.getY()- (pointerSize / 2), pointerSize, pointcolor));
			repaint();
		}
	}
	public void setPointerColor(Color pcolor){
		this.pointcolor = pcolor;
	}
	public void setPointSize(int pointerSize){
		this.pointerSize = pointerSize;
	}
	
//Vous la connaissez maintenant, celle-là
	public void paintComponent(Graphics g) {
		 super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		
//Si on doit effacer, on ne passe pas dans le else => pas de dessin
		if(this.erasing){
			this.erasing = false;
		}
		
		else{
			//On parcourt notre collection de points
			for(Point p : this.points)
			{	
				g2.setColor(p.getColor());
				g2.drawOval(p.getX(), p.getY(), p.getSize(), p.getSize());
				g2.fillOval(p.getX(), p.getY(), p.getSize(), p.getSize());
				}
			}
		
		g2.dispose();
	}	
	
//Efface le contenu
	public void erase(){
		this.erasing = true;
		this.points = new ArrayList<Point>();
		repaint();
	}
}

/**********************************************************/
	public static void main(String [] args){
	
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
		
			public void run(){
				new EXO();			
			}	
		});	
	}

}
