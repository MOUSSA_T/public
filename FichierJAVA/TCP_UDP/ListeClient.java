import java.awt.List;
import java.io.*;
import java.net.*;
import java.util.ArrayList;

class ListeClient implements Runnable{
    Socket s;
    MonBD MBD;

    ListeClient(Socket s,MonBD MBD ){
	this.s              = s;
	this.MBD            = MBD;
    }
    public void run(){

        while(true){
            try{
                DataOutputStream outToClient = new DataOutputStream(s.getOutputStream());
                outToClient.writeBytes(MBD.get_Tab()+'\n');
                Thread.sleep(30000);
            }catch(Exception e){
		  	e.printStackTrace();
                }
        }
    }
}
