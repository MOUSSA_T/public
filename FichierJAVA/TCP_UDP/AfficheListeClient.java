import java.io.*;
import java.util.*;
import java.net.*;

class AfficheListeClient implements Runnable{
    Socket s;
    BufferedReader inFromServer;
    String str[];
    int cas;
  
    
    AfficheListeClient(Socket s, BufferedReader inFromServer,int cas){
        this.s            = s;
        this.inFromServer = inFromServer;
        this.cas          = cas;
    }
    String setSTR(int i,int p){
        return str[p*i];
    }
    
    public void run(){
	  String sentenceRecu;
	try{
	    while(true){
		 sentenceRecu = inFromServer.readLine();
		 str = sentenceRecu.split(" ");
            if(cas==1){
                for(int i=1;i<=(int)(str.length/4);i++)
                    System.out.println("Client "+i+". @IP: "+str[4*i-2]+" Port: "+str[4*i]);
            }
            else{
                for(int i=1;i<=(int)(str.length/4);i++)
                    System.out.println("Groupe "+i+". @IP: "+str[4*i-2]+" Port: "+str[4*i]);
            }
		 Thread.sleep(30000);
	    }
	}catch(Exception e){
	    e.printStackTrace();
	}	
    }
}
