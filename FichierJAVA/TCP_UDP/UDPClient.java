import java.io.*;
import java.util.*;
import java.net.*;



class UDPClient extends Thread {
	private final static BufferedReader ms = new BufferedReader(new InputStreamReader(System.in));
	private int portEcoute;                             // port pour envoyer/recevoir des datagrammes 
	String IPaddress= ("127.0.0.1");      // IP pour envoyer desdatagrammes
    protected volatile boolean running = true;
    
	// construct, parameter is command line parameters
	public UDPClient(int port,int portEcoute) throws Exception
	{
	System.out.println("Attente de la reponce du client pour commencer le chat" + InetAddress.getLocalHost()+ " Port " + port );
        //System.out.println("connection atablie avec le client: IP "+ InetAddress.getLocalHost()+ " Port " + port );
	this.portEcoute=portEcoute;

	
        start();// commence le thread pour recevoir et display les datagrams

//avertir le client de votre presence(connection)
        String mes="";
        mes+="connection atablie avec le client: IP : "+InetAddress.getByName(IPaddress)+" Port: "+portEcoute;

        byte[] data1 = mes.getBytes();                                     // convert to byte array
	DatagramSocket theSocket1 = new DatagramSocket();              // create datagram socket and the datagram
	DatagramPacket   theOutput1 = new DatagramPacket(data1, data1.length, InetAddress.getByName(IPaddress), port);
        
      theSocket1.send(theOutput1); 

        int comp;

	// attente de d'entree clavier, envois de datagram a l'adresse IP                
	while(running)
	try
	{
        String ns = ms.readLine();                       // lire un String
        byte[] data = ns.getBytes();                                     // convert to byte array
        DatagramSocket theSocket = new DatagramSocket();              // create datagram socket and the datagram
        DatagramPacket   theOutput = new DatagramPacket(data, data.length, InetAddress.getByName(IPaddress), port);
         comp = ns.compareTo("quiter");
        if(comp == 0) {
            theSocket.send(theOutput);
            //System.exit(0);
            running = false;
        }
        else
            theSocket.send(theOutput);                                      // and send the datagram
	}
	catch (Exception ex) {System.out.println("Erreur d'envois de datagram " + ex);}


}

  
// methode thread run 
    public void run()
    {
        int comparaison;
        try
        {
            // ovrir DatagramSocket pour recevoir
            DatagramSocket ds = new DatagramSocket(portEcoute);
            // loop forever reading datagrams from the DatagramSocket

            while (running)
            {
                byte[] buffer = new byte[65507];                       // array to put datagrams in
                DatagramPacket dt = new DatagramPacket(buffer, buffer.length); // DatagramPacket to hold the datagram
                ds.receive(dt);                                     // wait for next datagram
                String ns = new String(dt.getData(),0,dt.getLength());        // get contenets as a String
                comparaison = ns.compareTo("quiter");
                if(comparaison == 0) {
                    System.out.println("recu: votre interlocuteur a quiter" + ns );
                    // System.exit(0);
                    running = false;
                }
                else
                    System.out.println("recu: " + ns );
        

            }
        }catch (SocketException sep) {
            System.err.println("erreur de chat " + sep);
        }
        catch (IOException sep) {
            System.err.println("erreur de chat " + sep);
        }
        System.exit(1);                                                       // exit on error
    }
}

