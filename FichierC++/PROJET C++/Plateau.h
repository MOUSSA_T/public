#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

#define JAUNEG  "\033[1;33m"
#define JAUNED  "\033[0m"

#define VIOLLETG "\033[1;34m"
#define VIOLLETD "\033[0m"

#define VERTG "\033[1;31m"
#define VERTD "\033[0m"

#define ROUGEG "\033[1;32m"
#define ROUGED "\033[0m"

class Plateau{
public:
     Plateau(int taille);
     void set_grille(int lig,int col,char jeton);

	virtual void affiche()=0;
     virtual ~Plateau();
     virtual void Jouer()=0;


protected:
        int SIZE;
        char **grille;
};


#endif // PLATEAU_H_INCLUDED
