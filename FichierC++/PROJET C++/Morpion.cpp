#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include "Morpion.h"
#define DIM 3



using namespace std;

Morpion::Morpion(int taille):Plateau(taille){
//on seulement le paramettre taille pour obtenir le type de tableau (3x3) qu'on veut
}
//------------------------------------------------------------------------------------------
Morpion::~Morpion(){
//cette destructeur est virtuelle et ferq appel a la bonne destruction
}
//---------------------------------------------------------------------------------------------
void Morpion::affiche(){

 int i,j;


   cout<<endl<<"\t\t\t         ";
	   for(int i=0;i<DIM;i++) cout<<"   "<<i+1;
   for (i=0 ; i<DIM ; i++ ) {

	   cout<<endl<<"\t\t\t\t  +";
	   for(int i=0;i<DIM;i++)cout<<"---+";

        cout<<endl<<"\t\t\t\t  |";
      for(int i=0;i<DIM;i++) cout << "   |";

      cout<<endl<<"\t\t\t\t";
      cout<<i+1<<" |";

   	for ( j=0 ; j<DIM ; j++ ) {
		if (grille[i][j]=='O') {
			cout<<" "<<JAUNEG<<grille[i][j]<<JAUNED<<" |";
		}
		else {
			cout<<" "<<VIOLLETG<<grille[i][j]<<VIOLLETD<<" |";
		}
      }
      cout<<" "<<i+1;
      cout<<endl<<"\t\t\t\t  |";
      for(int i=0;i<DIM;i++) cout << "   |";
   }
       cout<<endl<<"\t\t\t\t  +";
	   for(int i=0;i<DIM;i++)cout<<"---+";

        cout<<endl<<"\t\t\t         ";
	   for(int i=0;i<DIM;i++) cout<<"   "<<i+1;


}




//-------------------------------------------------------------------------------------------------------
/*
char Morpion::getCase(int x, int y){
    return grille[x][y];
}*/
//------------------------------------------------------------------------------------------------------
bool Morpion::aDesCasesVides(){


    int compt=DIM*DIM;
    for ( int i=0 ; i<DIM ; i++ ) {
        for (int j=0 ; j<DIM ; j++ ) {
            if(grille[i][j]!= ' '){
				--compt;
            }
        }
   }
 //cout<<endl<<endl<<"nombre de case vide: "<<compt<<endl;
   if(compt==0) return false;

  return true;
}

//----------------------------------------------------------------------------------------------------
int Morpion::testGagnant(char jeton){



    char gagner=' ';
   // char possibilite = (jeton == 'O')? 'X' : 'O';
    int nombDeCaseVide=0;
    for(int i=0;i<DIM;i++){
            if( (grille[i][0]==jeton)  && (grille[i][1]==jeton) && (grille[i][2]==jeton) ){
                gagner=jeton;
               // cout<<"il existe un alignement vertical"<<endl;

            }

             if( (grille[0][i]==jeton) && (grille[1][i]==jeton) && (grille[2][i]==jeton) ){
                gagner=jeton;
            //cout<<"il existe un alignement horizontal"<<endl;
            }
    }

        if( (grille[0][0]==jeton) && (grille[1][1]==jeton) && (grille[2][2]==jeton) ){
            gagner=jeton;
            //cout<<"il existe un alignement diagonal droite"<<endl;
        }

        if( (grille[0][2]==jeton) && (grille[1][1]==jeton) && (grille[2][0]==jeton) ){
            gagner=jeton;
            //cout<<"il existe un alignement diagonal gauche"<<endl;
        }

          // Recherche des cases vides
          for ( int i=0 ; i<DIM ; i++ ) {
        for (int j=0 ; j<DIM ; j++ ) {
            if(grille[i][j]== ' '){
				nombDeCaseVide ++;
            }
        }
   }


   if ( (nombDeCaseVide == 0) && (gagner == ' ' ) ){
   	gagner = 'N';//il ya ùatch null car la partie est terminee et il n'y aps de vaniqueur
   }

   return gagner;

}
//------------------------------------------------------------------------------------------------------

void Jeu(Morpion &jeu){

   cout<<"\t\t###############################################"<<
   endl<<"\t\t#*********************************************#"<<
   endl<<"\t\t#*************                   *************#"<<
   endl<<"\t\t#*************      MORPION      *************#"<<
   endl<<"\t\t#*************                   *************#"<<
   endl<<"\t\t#*********************************************#"<<
   endl<<"\t\t###############################################"<<endl;

   cout<<"joueur 1: O"<<endl
       <<"joueur 2: X"<<endl;

   jeu.affiche();

   int x,y;
   int joueur=0;
   char g=' ';

  do{

   	if(joueur++% 2){
            do{

              cout<<endl<<endl<<VIOLLETG<<"Joueur X"<<VIOLLETD<<endl;
              cout<<"ligne: ";
              cin>>x;
              cout<<"colone: ";
              cin>>y;
              if(x<=0||x>3||y<=0||y>3)

                cout<<"cout non valide!!!"<<endl;

              }while(x<=0||x>3||y<=0||y>3);

            jeu.set_grille(--x,--y,'X');
            fflush(stdin);
            g=jeu.testGagnant('X');
            //cout<<endl<<jeu.testGagnant('X');
              }

      else{
                do{


                  cout<<endl<<endl<<JAUNEG<<"Joueur O"<<JAUNED<<endl;
                  cout<<"ligne: ";
                  cin>>x;
                  cout<<"colone: ";
                  cin>>y;
                  if(x<=0||x>3||y<=0||y>3)

                    cout<<"cout non valide!!!"<<endl;

                  }while(x<=0||x>3||y<=0||y>3);

                jeu.set_grille(--x,--y,'O');
                fflush(stdin);
                g=jeu.testGagnant('O');

      }


          jeu.affiche();


   } while(g==' ' && jeu.aDesCasesVides());

   switch(g){
   	case 'O' :
      		cout<<endl<<"joueur"<<JAUNEG <<" O "<<JAUNED<<"a gagner"<<endl;
            break;
    case 'X':
      		cout<<endl<<"joueur"<<VIOLLETG <<" X "<<VIOLLETD<<"a gagner"<<endl;
	   		break;
    default:
      		cout<<endl<<ROUGEG<<"macht nul!!!"<<ROUGED<<endl;
            break;
   }

   fflush(stdin);
}

void Morpion::Jouer(){
    Morpion pp;
    for(int i=0;i<DIM;i++)
        for(int j=0;j<DIM;j++)
            pp.set_grille(i,j,grille[i][j]);

            Jeu(pp);
}
