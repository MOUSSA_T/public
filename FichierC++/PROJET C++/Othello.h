#ifndef OTHELLO_H_INCLUDED
#define OTHELLO_H_INCLUDED
#include "Plateau.h"
#define DIM 8

class Othello:public Plateau{
public:
    Othello(int taille=8);
    virtual ~Othello();

    int deplacement_valide(int deplacement[][DIM], char jeton);
    void echange_de_jeton( int lig, int col, char jeton);
    void deplacement_ordi( int deplacement[][DIM], char jeton);
    int meilleur_deplacement(int deplacement[][DIM], char jeton);
    int get_score(char jeton);
    bool in_tab(int lig,int col,char jeton);
	virtual void affiche();
    void Jouer();

};

void Jeu(Othello &p);




#endif // OTHELLO_H_INCLUDED
