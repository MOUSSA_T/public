#ifndef MORPION_H_INCLUDED
#define MORPION_H_INCLUDED
#include "Plateau.h"

class Morpion:public Plateau{
    public:
            Morpion(int taille=3);
            virtual ~Morpion();

			virtual void affiche();
            //void set_grille(int,int,char);
            char getCase(int,int);
            int testGagnant(char);
            bool aDesCasesVides();
            void Jouer();

};

void Jeu(Morpion &jeu);

#endif // MORPION_H_INCLUDED
