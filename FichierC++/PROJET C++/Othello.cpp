#include <iostream>
#include "Othello.h"
#include <stdio.h>
#include <ctype.h>
#define DIM 8
using namespace std;





Othello::Othello(int taille):Plateau(taille){
//on seulement le paramettre taille pour obtenir le type de tableau (8x8) qu'on veut
}


//----------------------------------------------------------
Othello::~Othello(){
//cette destructeur est virtuelle et ferq appel a la bonne destruction
}


//---------------------------------------------------------------------------------------------------------
bool Othello::in_tab(int i,int j,char jeton){
return grille[i][j]==jeton;

}


//-----------------------------------------------------------------------------
int Othello::deplacement_valide( int deplacement[][DIM], char jeton)
{

   int x = 0;
   int y = 0;
   int num_de_deplacement = 0;  /* Numero de deplacement valide            */

   /* on donne les cas possibles  */
   char jeton_advers = (jeton == 'O')? 'X' : 'O';

   /* On initialise la liste des mouvement a {0} */
   for(int lig = 0; lig < DIM; lig++)
     for(int col = 0; col < DIM; col++)
       deplacement[lig][col] = 0;

   /* trouver les cases pour fair des daplacement valides .                           */
   for(int lig = 0; lig < DIM; lig++)
     for(int col = 0; col < DIM; col++)
     {
       if(grille[lig][col] != ' ')
         continue;

       /* on verifie pour toutes les directions possibles ou peut fair des deplacement*/
       for(int _lig = -1; _lig <= 1; _lig++)
         for(int _col = -1; _col <= 1; _col++)
         {
           /* Pour ne pas sortir du grille */
           if(lig+ _lig < 0 || lig + _lig >= DIM ||
              col + _col < 0 || col + _col >= DIM ||
                                       (_lig==0 && _col==0))
             continue;

         /* on se touve dans le grille        */

           if(grille[lig + _lig][col + _col] == jeton_advers)
           {

             x = lig + _lig;
             y = col + _col;


             for(;;)
             {
               x += _lig ;
               y += _col;

               /* si on sort du tableau, arret */
               if(x < 0 || x >= DIM || y < 0 || y >= DIM)
                 break;

               /* Si on trouve une case vide, arret */
               if(grille[x][y] == ' ')
                 break;
                /*  Si la case a un pion, alors nous avons un deplacemnt valide */
               if(grille[x][y] == jeton)
               {
                 deplacement[lig][col] = 1;
                  num_de_deplacement++;
                 break;
               }
             }
           }
         }
     }
   return  num_de_deplacement;
}


//-----------------------------------------------------------------------------------
void Othello::deplacement_ordi( int deplacement[][DIM], char jeton)
{

   int meil_lig = 0;
   int meil_col = 0;
   int nouv_score = 0;
   int score = 100;

   Othello pp;      /* copie local du tableau    */

  // int locale_deplacemt[DIM][DIM];           /* deplacement local */
   char jeton_advers = (jeton == 'O')? 'X' : 'O'; /* identification du jeton */


   for(int lig = 0; lig < DIM; lig++)
     for(int col = 0; col < DIM; col++)
     {
       if(deplacement[lig][col] == 0)
         continue;


       for(int i = 0; i < DIM; i++)
         for(int j = 0; j < DIM; j++)
           pp.set_grille(i,j,grille[i][j]);


       pp.echange_de_jeton( lig, col, jeton);


       pp.deplacement_valide( deplacement, jeton_advers);

       nouv_score = pp.meilleur_deplacement( deplacement, jeton_advers);

       if(nouv_score<score)
       {
         score = nouv_score;
         meil_lig = lig;
         meil_col = col;
       }
     }


   echange_de_jeton( meil_lig, meil_col, jeton);
}

//-------------------------------------------------------------------
int Othello::get_score( char jeton)
{
   int score = 0;

   char jeton_advers = jeton == 'O' ? 'X' : 'O';


   for(int lig = 0; lig < SIZE; lig++)
     for(int col = 0; col < SIZE; col++)
   {
     score -= grille[lig][col] == jeton_advers;
     score += grille[lig][col] == jeton;
   }
   return score;
}

//----------------------------------------------------------------------------


/*
    Le meilleur deplacement consiste a trouver le plus pions advers qu'on peut
    encadrer et puis de returner ce nombre
*/
int Othello::meilleur_deplacement( int deplacement[][DIM], char jeton)
{
    Othello pp;
   //char jeton_advers = jeton=='O'?'X':'O';

   int score = 0;
   int nouv_score = 0;


   for(int lig = 0 ; lig<DIM ; lig++)
     for(int col = 0 ; col<DIM ; col++)
     {
       if(!deplacement[lig][col])
         continue;


       for(int i = 0 ; i<DIM ; i++)
         for(int j = 0 ; j<DIM ; j++)
           pp.set_grille(i,j,grille[i][j]) ;


       pp.echange_de_jeton( lig, col, jeton);


       nouv_score = pp.get_score( jeton);

       if(score<nouv_score)
               score = nouv_score;
     }
   return score;
}


//----------------------------------------------------------------------
void Othello::echange_de_jeton( int lig, int col, char jeton)
{
   int x = 0;
   int y = 0;
   char jeton_advers = (jeton == 'O')? 'X' : 'O';
   grille[lig][col] = jeton;

/*
    Pour chaque position on fait un deplacement suivant 4 directions,
    qui indiquent donc 8 position dans les quelles on peut placer un
    pion:
    {1,0}, {1,1}, {0,1}, {-1,1}, {-1,0}, {-1,-1}, {0,-1}, {1,-1}
*/

   for(int _lig = -1; _lig <= 1; _lig++)
     for(int _col = -1; _col <= 1; _col++)
     {

       if(lig + _lig < 0 || lig + _lig >= DIM ||
          col + _col < 0 || col + _col >= DIM || (_lig==0 && _col== 0))
         continue;//car on ne peut pas placer un pion qui est en dehors du tableau


       if(grille[lig + _lig][col + _col] == jeton_advers)
       {

         x = lig + _lig;
         y = col + _col;

         for(;;)
         {
           x += _lig;
           y += _col;


           if(x < 0 || x >= DIM || y < 0 || y >= DIM)
             break;


           if(grille[x][y] == ' ')
             break;


           if(grille[x][y] == jeton)
           {
             while(grille[x-=_lig][y-=_col]==jeton_advers)
               grille[x][y] = jeton;
             break;
           }
         }
       }
     }
}
//-----------------------------------------------------------------------------------------------------
void Othello::affiche(){
	

        cout<<endl<<endl<<"\t\t    ";
        for(int i(0);i<SIZE;i++){
            cout<<i+1<<"   ";
        }
        cout<<endl;
            //affichage du tableau
           for(int lig = 0; lig < SIZE; lig++)
           {
             cout<<"\t\t  +";
             for(int col = 0; col < SIZE; col++){
                cout<<"---+";
             }

             cout<<endl<<"\t\t"<<lig + 1<<" |";

             for(int col=0; col < SIZE; col++){
                   //affichage de chaque ligne
             }
			for (int col=0; col < SIZE; col++ ) {
				if (grille[lig][col]=='O') {
					cout<<" "<<JAUNEG<<grille[lig][col]<<JAUNED<<" |";
				}
				else {
					cout<<" "<<VIOLLETG<<grille[lig][col]<<VIOLLETD<<" |";
					//affichage de chaque ligne
				}
      		}//affichage de chaque ligne
             cout<<" "<<lig+1<<endl;

           }

           cout<<"\t\t  +";
           for(int col = 0 ; col<SIZE ;col++)
             cout<<"---+";
           cout<<endl;
           cout<<"\t\t    ";
        for(int i(0);i<SIZE;i++){
            cout<<i+1<<"   ";
        }
        cout<<endl;
   }




//---------------------------------------------------------------------------------------------
void Jeu(Othello &p){
	int deplacement[DIM][DIM];

	for(int ii=0; ii<DIM;ii++){
		for (int jj = 0; jj< DIM ; jj++) {
			deplacement[ii][jj] = 0;
		}
	}
    /* deplacement valide          */

  int num_de_jeu = 0;              /* Numero du jeux     */
  int nombre_de_jeton = 0;              /* Numero de deplacement      */
  int invalide_deplacement = 0;
  int score_joueur2 = 0;
  int score_joueur1 = 0;
  int y = 0;                       /*  Entree colone       */
  int x = 0;                        /* Entree ligne           */
  int num_jeton = 0;                   /*Indicateur du numero de jeton    */
  int choix=0;

   cout<<"\t\t###############################################"<<
   endl<<"\t\t#*********************************************#"<<
   endl<<"\t\t#*************                   *************#"<<
   endl<<"\t\t#*************      OTHELLO      *************#"<<
   endl<<"\t\t#*************                   *************#"<<
   endl<<"\t\t#*********************************************#"<<
   endl<<"\t\t###############################################"<<endl;


   cout<<endl<<"\t\t       *~~~~~~~* MODE DE JEU *~~~~~~~*         "<<endl;
   cout<<endl<<"\t\t1.joueur vs joueur"<<endl
       <<"\t\t2.joueur vs ordinateur"<<endl<<endl<<endl
       <<"Quelle est votre choix: ";
   cin>>choix;

   cout<<endl<<"Pour jouer a ce jeu,"<<endl
       <<"il faut entre la coordonnee "<<endl
       <<"de la ligne puis celle de la colone "<<endl<<endl;
   cout<<" Exemple:"<<endl;
   cout<<"entre ligne: "<<endl
       <<"entre colone: "<<endl<<endl;

   cout<<"joueur 1: O"<<endl
       <<"joueur 2: X"<<endl<<endl;

   cout<<"\t\t   BONNE CHANCE!!!\t\t"<<endl;




     num_jeton = ++num_de_jeu % 2;
     nombre_de_jeton = 4;

     //Placer les quatres premier pion sur le tableau
     p.set_grille(DIM/2 - 1,DIM/2 - 1,'O');
     p.set_grille(DIM/2,DIM/2,'O');
     p.set_grille(DIM/2 - 1,DIM/2,'X');
     p.set_grille(DIM/2,DIM/2 - 1,'X');


     do
     {


switch(choix){

    case 1:{
    p.affiche();             /* affichage du tableau  */
       if(num_jeton++ % 2)
       {   /* c'est autour du joueur 2 de jouer           */
         if(p.deplacement_valide( deplacement, 'O'))
         {
         cout<<JAUNEG<<"joueur O"<<JAUNED<<endl;

           for(;;)
           {

             fflush(stdin);              /* vider la cage de l'entree precedente */
             cout<<"Entrez votre deplacement: "<<endl;
             cout<<"ligne: ";
             cin>>x;
             cout<<"colone: ";
             cin>>y;

             x--;
             y--;
             if( x>=0 && y>=0 && x<DIM && y<DIM && deplacement[x][y])
             {
               p.echange_de_jeton(x, y, 'O');
               nombre_de_jeton++;
               break;
             }
             else
               cout<<"deplacement invalide!."<<endl<<endl;
           }
         }
         else
           if(++invalide_deplacement<2)
           {
             fflush(stdin);
             cout<<"ton tour vas passer, press return";

           }
           else
             cout<<endl<<"le jeu est bloquer, la partie est terminee."<<endl;
       }
       else
       {
  /* c'est autour du joueur 2 de jouer           */
         if(p.deplacement_valide( deplacement, 'X'))
         {
         cout<<VIOLLETG<<"joueur X"<<VIOLLETD<<endl;

    for(;;)
           {

             fflush(stdin);             /* vider la cage de l'entree precedente */
             cout<<"Entrez votre deplacement: "<<endl;
             cout<<"ligne: ";
             cin>>x;
             cout<<"colone: ";
             cin>>y;

             x--;
             y--;
             if( x>=0 && y>=0 && x<DIM && y<DIM && deplacement[x][y])
             {
               p.echange_de_jeton(x, y, 'X');
               nombre_de_jeton++;
               break;
             }
             else
               cout<<"deplacement invalide!"<<endl<<endl;
           }
         }
         else
           if(++invalide_deplacement<2)
           {
             fflush(stdin);
             cout<<"ton tour est passe";
//
           }
           else
             cout<<endl<<"Le jeu est blocquer, la partie est terminee."<<endl;

       }

    }
        break;
   /*************************************************************************************/



    case 2:{
    p.affiche();             /* affiche tableau */
       if(num_jeton++ % 2)
       { /*    c'est autour du jour 1 de jouer           */
            if(p.deplacement_valide( deplacement, 'O'))
            {
            cout<<JAUNEG<<"joueur O"<<JAUNED<<endl;

                for(;;)
                {

                    fflush(stdin);              /* vider la cage de l'entree precedente */
                    cout<<"Entrez votre deplacement: "<<endl;
                    cout<<"ligne: ";
                    cin>>x;
                    cout<<"colone: ";
                    cin>>y;

                     x--;
                     y--;
                     if( x>=0 && y>=0 && x<DIM && y<DIM && deplacement[x][y])
                     {
                       p.echange_de_jeton(x, y, 'O');
                       nombre_de_jeton++;
                       break;
                     }
                     else
                       cout<<"deplacement invalide!"<<endl<<endl;
                }
            }
            else
                if(++invalide_deplacement<2)
                       {
                         fflush(stdin);
                         cout<<"ton tour vas passer";

                       }
                else
                         cout<<endl<<"le jeu est bloquer, la partie est terminee."<<endl;
       }
       else
       { /* c'est autour de l'ordinateur de jouer           */
                if(p.deplacement_valide( deplacement, 'X'))
                {
                       invalide_deplacement = 0;               /* mettre a zero invalid_deplacemt   */
                       p.deplacement_ordi( deplacement, 'X');
                       nombre_de_jeton++;                   /* Incrementation du deplacement  */
                }
                else
                {
                       if(++invalide_deplacement<2)
                         cout<<endl<<"Je passe, c'est ton tour"<<endl; /* deplacement non valide */
                       else
                         cout<<"le jeu est bloquer, la partie est terminee."<<endl;
                }
       }
    }

        break;
/********************************************************************************************************/

     default:
     cout<<"Faites votre choix svp!!!"<<endl;
     cin>>choix;
     break;
/****************************************************************************************************************/
}



     }while(nombre_de_jeton< DIM*DIM && invalide_deplacement<2);

     //partie terminer
     p.affiche();  //affichage final du tableau;

    //faire le decompte
    score_joueur2 = score_joueur1 = 0;
     for(int lig = 0; lig < DIM; lig++)
       for(int col = 0; col < DIM; col++)
       {

         if (p.in_tab(lig,col,'X'))score_joueur2++;
         else score_joueur1 ++ ;
       }
     cout<<"Score final:"<<endl;
     cout<<JAUNEG<<"Joueur1 (O): "<<JAUNED<< score_joueur1<<endl;
     cout<<VIOLLETG<<"Joueur2 (X): "<<VIOLLETD<<score_joueur2;
      cout<<endl<<endl;

     fflush(stdin);




}
//---------------------------------------------------------------------
void Othello::Jouer(){
Othello pp;
    for(int i=0;i<DIM;i++)
        for(int j=0;j<DIM; j++)
        pp.set_grille(i,j,grille[i][j]);

        Jeu(pp);
}
