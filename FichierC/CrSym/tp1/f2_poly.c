#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "f2_poly.h"

f2_deg_t 
f2_poly_deg(f2_poly_t polynome){
  
    int i,bin=0;
    f2_deg_t deg=0;
    f2_deg_t nb=64;
    char poly[nb];
    for(i=nb-1;i>=0;i--)
      {
        bin=(polynome>>i)&0x01;
	poly[i]=bin;
      }
    
    for(i=nb-1;i>=0;i--){
     
      if(poly[i]==1){
	 deg=i;
	break;
      }
     
    }  
    return (deg);
  }


int
f2_poly_print(f2_poly_t poly, char X, FILE *os){

    int i,bin=0;
    f2_deg_t nb=64;
    char polyn[nb];
    for(i=nb-1;i>=0;i--)
      {
        bin=(poly>>i)&0x01;
	
	polyn[i]=bin;
      }
      for(i=nb-1;i>=0;i--){
     
      if(polyn[i]==1){
	if(i!=0 && i!=1) fprintf(os,"%c^%d+",X,i);
	if(i==1) fprintf(os,"%c+",X);
	if(i==0) fprintf(os,"%d",1);
      }
     
    }
      

  return 0;
}


int
f2_poly_div(f2_poly_t *pol1, f2_poly_t *pol2, f2_poly_t poly1, f2_poly_t poly2){
  
  f2_deg_t degpoly1 = f2_poly_deg(poly1);
  f2_deg_t degpoly2 = f2_poly_deg(poly2);
  
  f2_deg_t poly_num,poly_den;
  
  if(degpoly1>degpoly2){
    poly_num=poly1;
    poly_den=poly2;
  }
  else{
    poly_num=poly2;
    poly_den=poly1;
  }

  if(f2_poly_deg(poly_den)==0){
    fprintf(stdout,"Division par le polynome nul!!!");
    return -1;
  }
  else{
  	*pol2 =  poly_num % poly_den;
  	*pol1 = (poly_num - *pol2)/poly_den;
  }
 	
  return 0;
}

f2_poly_t
f2_poly_rem(f2_poly_t poly1, f2_poly_t poly2){

  f2_deg_t degpoly1 = f2_poly_deg(poly1);
  f2_deg_t degpoly2 = f2_poly_deg(poly2);
  f2_poly_t reste_poly=0;
  
  if(degpoly1>degpoly2){
     reste_poly=poly1%poly2;
  }
  else{
     reste_poly=poly2%poly1;
  }
 	
  return reste_poly;

}

f2_poly_t
f2_poly_gcd(f2_poly_t poly1, f2_poly_t poly2){

  f2_poly_t q, r, s, t, tmp,u,v;
  
  u = 1;
  v = 0;
  s = 0;
  t = 1;
  while (poly2 > 0) {
    q = poly1 / poly2;
    r = f2_poly_rem(poly1, poly2);
    poly1 = poly2;
    poly2 = r;
    tmp = s;
    s = u - q * s;
    u = tmp;
    tmp = t;
    t = v - q * t;
    v = tmp;
  }
  return poly1;
}


f2_poly_t
f2_poly_x2n( f2_deg_t deg, f2_poly_t poly2){
	int i=0;
	f2_poly_t pol = 2;
	for(i=0;i<deg;i++){
		pol = f2_poly_times(pol,pol,poly2) ;
			
	}
return pol;
}

f2_poly_t
f2_poly_times(f2_poly_t poly1, f2_poly_t poly2, f2_poly_t poly3){
   
   f2_poly_t reste_poly=0;
   reste_poly= (poly1 * poly2) % poly3;
   return reste_poly;
}


f2_poly_t
f2_poly_xtimes(f2_poly_t poly1, f2_poly_t poly2){
	
	return f2_poly_times(2,poly1,poly2);		

}

int
f2_poly_irred(f2_poly_t poly){
	int test=0;
	f2_deg_t  degre = 0;
	f2_poly_t  poly_x2n  = 0;
	f2_poly_t xnp_x=0;
	int i=2, nombreDiviseurs=0;
	/* on calcule de dregre du polynome*/
	degre = f2_poly_deg(poly);
	
	/*on calcul X^(q^n)-X mod P=val*/
	poly_x2n   = f2_poly_x2n(degre,poly)+2;
		

	if(poly_x2n==1) {
		f2_poly_print(poly,'X',stdout);
		fprintf(stdout," n'est pas irreductible\n");
	}
	else{
		f2_deg_t *diviseur = malloc(degre*sizeof(*diviseur));
		/*enumeration des diviseur premiers de degre*/
  
  
   	 	while(i <= (int)(degre^(-1/2)))//Test de division par tout entier inférieur ou égal à la racine carrée du nombre
   			 {
   			 test=0;
   			   if((degre%i) != 0)//S'il y a un reste, on passe au diviseur suivant
   	     			i++;
   	   		
   	  		 else//S'il n'y a pas de reste, on enregistre la valeur du diviseur
   	   			{
   	     			//diviseur[nombreDiviseurs] = i;
   	     
   	     			xnp_x= f2_poly_x2n(degre/i,poly)+2;

   	     				if(f2_poly_gcd(poly,xnp_x)!=1){
   	     	 			test=1;
   	     				
   	     				}
   	     
   	     			nombreDiviseurs++;
   	     			degre /= i;//On recommence la boucle avec le nombre à décomposer divisé par son premier diviseur
   	     			i=2;
   	      
   	   			}printf("%d\n",i);
    		}
    			
    		if(test==1){
    			 f2_poly_print(poly,'X',stdout);
   	     		fprintf(stdout," n'est pas irreductible\n");	
    		}
    		else{
    			f2_poly_print(poly,'X',stdout);
    			fprintf(stdout," est irreductible\n");
	
		}
	
	
	}
	//free(diviseur);
return 0;

}





