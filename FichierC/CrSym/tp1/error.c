#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <error.h>

static char __buf_err[256];

void
error_set(const char *fun, const char *fmt, ...)
{
    va_list ap;
    size_t len =  strlen(fun) + 4;

    sprintf(__buf_err, "%s(): ", fun);
    va_start(ap, fmt);
    vsnprintf(__buf_err + len , sizeof(__buf_err) - len, fmt, ap);
    va_end(ap);
}

void
error_sys_set(const char *fun, const char *sys)
{
    snprintf(
	__buf_err, sizeof(__buf_err), "%s(): %s(): (%d, %s)", fun, sys, errno,
	strerror(errno)
	);
}

void
error_print(FILE *os)
{
    fprintf(os, "%s\n", __buf_err);
}
