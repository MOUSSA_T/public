#ifndef F2_POLY_H
#define F2_POLY_H

#include <stdio.h>
#include <inttypes.h>

typedef uint64_t f2_poly_t;
typedef uint8_t  f2_deg_t;
// l'ordre des coefficients polynômiaux est du poids fort au poids faible.

#define F2_VARN 'X'

/* Les polynômes sur F2 de degré < 64 sont représentés 
 par des entiers de 64 bits, les coefficients sont ordonnés du poids
 fort vers le poids faible. Ex. 0x82 : X^7+X.
*/

/* Calcul sur F_2[X], pour des polynômes de degré au plus 63 */
f2_deg_t
f2_poly_deg(f2_poly_t);

int
f2_poly_print(f2_poly_t, char, FILE *);
// écriture polynomiale usuelle, avec pour variable le second argument

int
f2_poly_div(f2_poly_t *,f2_poly_t *, f2_poly_t, f2_poly_t);
// (arg1, arg2) = (quotient, reste) de arg3 par arg4

f2_poly_t
f2_poly_rem(f2_poly_t, f2_poly_t);
// reste de arg1 par arg2

f2_poly_t
f2_poly_gcd(f2_poly_t, f2_poly_t);
// pgcd(arg1, arg2)


/* Pour tous les calculs modulo. On peut  quotienter par un
polynôme de degré jusqu'à 63. */

f2_poly_t
f2_poly_xtimes(f2_poly_t, f2_poly_t);
// retourne X*arg1 mod (arg2)

f2_poly_t
f2_poly_times(f2_poly_t, f2_poly_t, f2_poly_t);
// retourne arg1 * arg2 modulo  arg3

f2_poly_t
f2_poly_x2n( f2_deg_t, f2_poly_t);
// retourne X^{2^arg1} modulo arg 2

int
f2_poly_irred(f2_poly_t);
// vérifie si le polynôme arg1 est irréductible

f2_poly_t
f2_poly_random(f2_deg_t);
// retourne un polynôme tiré au hasard parmi les polynômes de degré < arg2


#endif /* F2_POLY_H */
