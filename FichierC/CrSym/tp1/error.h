#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>

void error_set(const char *, const char *, ...);
void error_sys_set(const char *, const char *);
void error_print(FILE *);

#endif /* ERROR_H */
