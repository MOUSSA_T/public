#include <unistd.h>
#include <fcntl.h>

#include "heys.h"
#include "fonctions.h"

int Couple_alea(block_t diff, int nb_cpl){
  int DataIN;
  int DataOUT;
  int i;
  block_t lecture;
  DataIN  = open("/dev/urandom",O_RDONLY );
  DataOUT = open("Donnees_Aleatoires",O_WRONLY);

  if(DataIN < 0) return -1;
  if(DataOUT < 0) return -1;
  
  for(i = 0; i < nb_cpl; i++){
    if(read(DataIN,&lecture,2)< 0) return -1;
       write(DataOUT,&lecture,2);
       lecture ^= diff;
       write(DataOUT,&lecture,2);
  }
  close(DataIN);
  close(DataOUT);
  return 0;
}

int clef_partielle(){
	int i;
	int j;
	int k;
	int r;
	for(i = 0; i < 16; i++)
		for(j = 0; j < 16; j++){
			k = i ^(j<<8);	
			r = test_clef(5000,k);
			printf("k = %x;\tresultat = %d\n",k,r);
		}
return 0;
}
