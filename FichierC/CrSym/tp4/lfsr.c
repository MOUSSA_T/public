#include <stdio.h>
#include <stdlib.h>
#include "lfsr.h"

int lfsr_container_init (container_t *cont, const maxlen_t leng_reg, maxlen_t leng_poly){
 
  cont->regist = leng_reg ;
  cont->poly   = leng_poly ;
  
  return 0; 
}


int lfsr_init (lfsr_t *lfsr, const byte_t pbyte, const byte_t lbyte, container_t *conteneur){
  lfsr->zero = pbyte;
  lfsr->len  = lbyte;
  lfsr->cont = conteneur;
  return 0;
}

void lfsr_inc (lfsr_t *lfsr){
  container_t *conten;
  int i=0,nb = 64,nbcoeff=0,inc=0;
  int bit=0;
  maxlen_t deg;
  maxlen_t polyfixe;
  char polynome[nb];
  char *coeff;
  polyfixe=lfsr->cont->poly;
   
    for(i=lfsr->len;i>=0;i--)
      {
        bit = ((lfsr->cont->poly) >> i)&0x01;
	polynome[i]        = bit;
	if( bit == 1 ){
	  coeff[nbcoeff]=i;
	  	nbcoeff++;
	}
      }

    do{
       for(i=0;i<nbcoeff;i++){
	 lfsr->cont->regist ^= (lfsr->cont->poly >> (lfsr->len - coeff[i]));
      }

      printf("%zu", lfsr->cont->regist&0x1);
      lfsr->cont->poly       = (lfsr->cont->poly >> 1) | ( lfsr->cont->regist << (lfsr->len-1));

      inc++;
    }while(inc!=lfsr->len);
    lfsr->cont->regist = MAXLEN ^ lfsr->cont->regist; 
  printf("\n");
}
