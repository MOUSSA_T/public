#include <stdio.h>
#include <stdlib.h>
#include "lfsr.h"

int main(){
  int          i          = 0;
  int          nb         = 64;
  int          prem_bit   = 4;
  int          bit        = 0;
  maxlen_t     le_poly    = 0x11b;
  maxlen_t     le_registe = MAXINT;
  maxlen_t     deg;
  container_t *contenaire = malloc(sizeof(MAXLEN));
  lfsr_t      *lfsr       = malloc(MAXLEN);
  /*je calcul le dergre du polynome*/
  for(i=nb-1;i>=0;i--)
      {
         bit=(( le_poly) >> i)&0x01;
	 if(bit==1){
	   deg=i;
	   break;
	 }
      }
 
    le_registe = LFSR_MASK(lfsr);

    lfsr_container_init( contenaire, le_registe,le_poly);
 
    lfsr_init(lfsr, prem_bit, deg, contenaire);
    
    lfsr_inc(lfsr);
  return 0;
}
