#include "Berlekamp_Massey.h"
#include <stdio.h>

#define taille 12

int BerleKampMassey( int  s[]){
  int i;
  int P[taille] = {1};//P[0]=1 et P[i] = 0 pour i=1 a taille-1
  int Q[taille] = {1};//Q[0]=1 et Q[i] = 0 pour i=1 a taille-1
  int T[taille];
  int L;
  int k;
  int m;
  int d;
  
  L = 0;
  k = 0;
  m = -1;
  printf("+---------------------------------------------------------------------------------+\n");
  printf("| k\td\t     P\t\tl\t     Q\t\tm\t     T\t\ts |\n");
printf("+---------------------------------------------------------------------------------+\n");
 printf("| \t\t     1\t\t0\t     1\t\t-1\t     \t\t  |\n");

  while(k < taille){
    printf("| %d\t",k);/*AFFICHER LES K*/
    d = 0;
    
    for(i = 0; i <= L; i++){
      d ^= P[i] & s[k-i];/* d += (c[i] * s[k-i]) % 2*/
    }
    
    if(d == 1){
      for(i = 0; i < taille; i++){
	T[i] = P[i];/*T(x) := P(x)*/
      }
      for(i = 0; i < taille - k + m; i++){
	P[i + k - m] ^= Q[i];/*P(x) := P(x) + Q(x) * X^(k-m) */
	
      }
      
      if(L <= (k>>1)){/* if 2 * lamda <= k */
	L = k +1 - L; /* lamda := k + 1 - lamda */
	
   
	m = k; /* m := k*/
	
	 for(i = 0; i < taille; i++)
	   Q[i] = T[i];
      }/*Q(x) := T(x)*/
      
    }
/*AFFICHER LES DONNES*/
     printf("%d\t",d);
    for(i =  taille-1; i >= 0  ; i--){
	printf("%d",P[i]);
      }
     printf("\t%d\t",L);
     for(i =  taille-1; i >= 0  ; i--){
	printf("%d",Q[i]);
      }
     printf("\t%d\t",m);
     for(i =  taille-1; i >= 0  ; i--){
	printf("%d",T[i]);
      }
     printf("\t%d",s[k]);
  k++;
  printf(" |\n");

  }
  printf("+---------------------------------------------------------------------------------+\n");
  return 0;
}

