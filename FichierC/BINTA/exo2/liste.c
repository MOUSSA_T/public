#include <stdio.h> 
#include <stdlib.h>
#include "liste.h" 
	 
	 
Liste *creerListe() { 
    Liste *L               = malloc(sizeof(*L)); 
    if (L == NULL){
        printf("erreur allocation ListeVide\n");
        exit(EXIT_FAILURE);    
    } 
    L->longueur            = 0; 
    L->tete                = NULL;
    L->queue               = NULL; 
 	return L; 
} 
	 
 
void ajoutListe(Liste *L, int element) { 
    Cellule *cellule; 

    cellule = malloc(sizeof(*cellule)); 
	if (L == NULL || cellule == NULL) {
	    printf("erreur allocation memoire \n");
	    exit(EXIT_FAILURE); 
	}
	
    cellule->element        = element; 
    cellule->suivant        = NULL; 
    
    if (L->longueur == 0){ 
        L->tete             = cellule;
        L->queue            = cellule;     
    } 
    
    else { 
        L->queue->suivant   = cellule; 
        L->queue            = cellule; 
	} 
	 ++(L->longueur); 
} 

	 
int  parcourListe(Liste *L) { 
    Cellule *cellule; 
    int element; 
    if (L == NULL || L->longueur == 0) printf("erreur liste n'existe pas\n"); 
    cellule                 = L->tete; 
    element                 = cellule->element; 
    if (L->longueur == 1){ 
        L->tete             = NULL;
        L->queue            = NULL; 
    } 
    else{ 
        L->tete             = L->tete->suivant; 
    } 
    /*free(cellule);*/ 
	--(L->longueur); 
	return(element); 
} 
 
void libererListe(Liste *liste){
    if(liste==NULL) exit(EXIT_FAILURE);
    while(liste->tete != NULL){
        Cellule *aSupprimer = liste->tete;
        liste->tete         = liste->tete->suivant;
        free(aSupprimer);
    }
}
