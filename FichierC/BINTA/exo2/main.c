#include <stdio.h> 
#include <stdlib.h> 
#include "graphe_liste.h" 

	 
#define N 10 
#define nombreArc 10

	 	 
int main(){ 
    int arc[10][2];
    int comp[N] ;
    Sommet* graphe = NULL;
    graphe         = CreerGraphe(N); 
    
    graphe         = CreerArete(graphe,0,3,N); 
	graphe         = CreerArete(graphe,0,7,N); 
	graphe         = CreerArete(graphe,0,9,N);
	graphe         = CreerArete(graphe,3,4,N);
	graphe         = CreerArete(graphe,4,8,N);   
	graphe         = CreerArete(graphe,7,8,N); 
	graphe         = CreerArete(graphe,9,2,N); 
	graphe         = CreerArete(graphe,2,5,N); 
	graphe         = CreerArete(graphe,1,6,N); 
	
	arc[0][0] = 0 ;
    arc[0][1] = 3 ;
    arc[1][0] = 0 ;
    arc[1][1] = 7 ;
    arc[2][0] = 0 ;
    arc[2][1] = 9 ;
    arc[3][0] = 3 ;
    arc[3][1] = 4 ;
    arc[4][0] = 4 ;
    arc[4][1] = 8 ;
    arc[5][0] = 7 ;
    arc[5][1] = 8 ;
    arc[6][0] = 9 ;
    arc[6][1] = 2 ;
    arc[7][0] = 2 ;
    arc[7][1] = 5 ;   
    arc[9][0] = 1 ;
    arc[9][1] = 6 ; 

	 
	afficheGraphe(graphe,N); 
	parcourLargeur(graphe,N);
   
    composanteConnexes(N,nombreArc,arc,comp);
    lesComposanteConnexes(comp,N);

    libererGraphe(graphe,N);
 
    return 0;
}


