#ifndef GRAPHE_LISTE_H
#define GRAPHE_LISTE_H
	
#include <stdio.h> 
#include <stdlib.h>
#include "liste.h"  
	 
typedef int bool;	 
	 
/*taille max de sommet*/ 
typedef struct __arete { 
    int             sommet; 
    int             valeur; 
    struct __arete* arete_suivant; 
}Arete; 
	 
typedef struct __noeud{ 
    Arete*          arete; 
    int             valeur; 
    char            couleur; 
}Noeud; 
	 
	 
typedef  Arete*     Arc; 
typedef  Noeud*     Sommet; 
	 
	 
Sommet* CreerGraphe(int); 
Sommet* CreerArete(Sommet* ,int,int ,int ); 
void    afficheGraphe(Sommet* , int ); 
void    parcourLargeur(Sommet*, int ); 
int     libererGraphe(Sommet *,int);
int     libereSommet(Sommet *,int);
void    composanteConnexes(int taille,int nombreArc,int arc[][2],int *);
void    lesComposanteConnexes(int *, int);

#endif /*GRAPHE_LISTE_H*/
