#ifndef LISTE_H
#define LISTE_H
 
typedef struct _cellule { 
    int element; 
    struct _cellule *suivant; 
}Cellule; 
 
	 
typedef struct _liste { 
    int longueur; 
    Cellule *tete; 
    Cellule *queue; 
}Liste; 
	 
Liste *creerListe(); 
void ajoutListe(Liste *, int); 
int parcourListe(Liste *); 
void libererListe(Liste *);


#endif /*LISTE_H*/
