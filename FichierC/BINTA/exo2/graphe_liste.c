#include "graphe_liste.h" 

#define true  1
#define false 0

	 
Sommet* CreerGraphe(int taille){ 
    int i; 
    Sommet* Newgraphe               = (Sommet*)malloc(taille*sizeof(Sommet)); 
    for(i=0;i<taille;i++){ 
        Newgraphe[i]                = (Sommet)malloc(sizeof( Sommet)); 
	    Newgraphe[i]->valeur        = i; 
	    Newgraphe[i]->arete         = NULL; 
	} 
	return Newgraphe; 
} 
	 
	 	  
Sommet* CreerArete(Sommet* graphe,int s1,int s2,int valeur){ 
    if(graphe[s1]->arete==NULL){  
	    Arc Newarc                  = (Arc)malloc(sizeof(Arete)); 
	    Newarc->valeur              = valeur; 
	    Newarc->sommet              = s2; 
	    Newarc->arete_suivant       = NULL; 
	    graphe[s1]->arete           = Newarc; 
	    return graphe; 
    } 
	 
	else{ 
	    Arc Newarc;
	    Arc temp                    = graphe[s1]->arete; 
	    while( !(temp->arete_suivant==NULL)){ 
	        temp=temp->arete_suivant; 
        } 
	     Newarc=(Arc)malloc(sizeof(Arete)); 
	     Newarc->valeur             = valeur; 
	     Newarc->sommet             = s2; 
	     Newarc->arete_suivant      = NULL; 
	 
	     if(temp->sommet>s2){ 
    	      Newarc->arete_suivant = temp->arete_suivant; 
              Newarc->sommet        = temp->sommet; 
    	      temp->sommet          = s2; 
              temp->arete_suivant   = Newarc; 
    	      return graphe; 
	     } 
	 
	     temp->arete_suivant=Newarc; 
	    return graphe; 
	} 
} 
	 
void afficheGraphe(Sommet* graphe, int taille){ 
    int i; 
    Arc temp; 
	 
	  for(i=0;i<taille;i++){ 
	    printf("%d",graphe[i]->valeur); 
	    if(!(graphe[i]->arete==NULL)){ 
	        printf(" <-> %d",graphe[i]->arete->sommet); 
	        temp                   = graphe[i]->arete; 
	      while(!(temp->arete_suivant == NULL)){ 
	        temp                   = temp->arete_suivant; 
	        printf(",%d",temp->sommet); 
	      } 
	    } 
	  printf("\n"); 
	  } 
} 
	 

	 
	 
	 
void parcourLargeur(Sommet* graphe, int taille){ 
	     /*Initialisation de la couleur des sommets 
	     N=Noir, deja vu.  B=Blanc a voir*/ 
	     int i; 
	     Liste *liste;
	     printf("\n\t***PARCOURS EN LARGEUR***\n"); 
	     liste                     = creerListe();
	     for(i=0;i<taille;i++){ 	       
	        graphe[i]->couleur     ='B'; 
	     } 
	     ajoutListe(liste,0); 
	     while(liste->longueur){ 
	       i                       = parcourListe(liste); 
	       if(graphe[i]->couleur=='B'){ 
	         Arc TemArc            = graphe[i]->arete; 
	         if(TemArc!=NULL){ 
	           ajoutListe(liste,TemArc->sommet); 
	           while(TemArc->arete_suivant!=NULL){ 
	             TemArc            = TemArc->arete_suivant; 
	             ajoutListe(liste,TemArc->sommet); 
	           } 
	         } 
	       printf("%d ",graphe[i]->valeur); 
	       graphe[i]->couleur      ='N'; 
	       } 
    } 
    libererListe(liste);
    printf("\n\n"); 
} 


int libereSommet(Sommet *n,int taille){
  int i;
  for(i=0; i<taille ;i++){
    if((n[i]->arete)!=NULL){
      /*libereSommet(n[i]->arete);*/
      free(n[i]->arete);
    }
  }
  free(n);
  return 1;
}

int libererGraphe(Sommet *g, int taille)
{
  if(g != NULL)
    {
      libereSommet(g,taille);
    }
  

  return 1;
}

void composanteConnexes(int taille,int nombreArc,int arc[][2],int *comp){

    int x,y,c,i,j,k;

    for(i=0;i<taille;i++){
        comp[i] =i;
    }
    
    for(j=0; j< nombreArc; j++){
        x       = arc[j][0];
        y       = arc[j][1];
        
        if(comp[x] != comp[y]){
            c   = comp[y];
            for(k = 0; k < taille;k++){
                if(comp[k]==c) 
                    comp[k] = comp[x];
            }
        }
    }  
}

void lesComposanteConnexes(int *comp, int taille){
    int i,j,k = 0;
    fprintf(stdout,"\t***COMPOSANTES CONNEXES***\n");
    for(j = 0; j<taille;j++){
        if(comp[j]==j){
            fprintf(stdout,"composante connexe pour le sommet %d:",j);
            for(i=0; i < taille; i++){
                if(comp[i] == j)
                    fprintf(stdout," %d",i); 
                
            }
            k++;
            fprintf(stdout,"\n");
        }
    }
     fprintf(stdout,"il y a au total %d composante(s) connexe(s)\n\n\n",k);

}
