#include <stdio.h>
#include <stdlib.h>
#include "tri_tas.h"



int main(int argc, char **argv){

    FILE *lecture_fichier = NULL;
    int *tab = NULL;
    int taille =0;
    if(argc != 2){
        fprintf(stdout,"erreur <./Tripartas> <fichier.txt>\n");
    }
    else{
        lecture_fichier     = fopen(argv[1],"r");
        if(lecture_fichier == NULL){/*on verifie si le fichier a bien été ouvert*/
                fprintf(stderr,"erreur!!! \nce fichier n'existe pas!\n");
        }
        else{
    
            tab = calloc(taille,sizeof(int));
            if(tab != NULL){/*on verifie si de la memoire a été bien alloué pour le tableau */
                copie_tab(lecture_fichier, tab,&taille);/*copie des données du fichier*/   
                printf("Avant le tri\n");
                affiche_tab(tab,taille);   
                 tri_par_tas(tab,taille);
                printf("Apres le tri\n");
                affiche_tab(tab,taille);
                free(tab);
                fclose(lecture_fichier);
                 }
                else{
                    fprintf(stdout,"memoire insuffisante! \n");
                }
            }
       
    } 
    return 0;
}
