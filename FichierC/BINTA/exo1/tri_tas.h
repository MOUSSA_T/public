#ifndef _TRI_TAS_
#define _TRI_TAS_

#include <stdio.h>

void permuter(int *, int *);
void tamiser(int *, int, int);
void tri_par_tas(int *, int);
void affiche_tab(int *, int);
void copie_tab(FILE *,int *,int *);

#endif /*_TRI_TAS_*/
