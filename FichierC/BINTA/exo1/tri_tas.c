#include "tri_tas.h"
#include <stdio.h>

void permuter(int *x, int *y){
    int z = *x;
    *x    = *y;
    *y    = z;
}


void tamiser(int *tab, int noeud, int taille){
    int k = noeud;
    int j = 2 * k;
    while(j <= taille){
        if(j < taille && tab[j] < tab[j+1])
            j++;
        
        if(tab[k] < tab[j]){
            permuter(&tab[k],&tab[j]);
            k = j;
            j = 2 * k;
        }    
        else break;
    }
}

void tri_par_tas(int *tab,int taille){
    int i = 0;
    for(i = taille/2; i >= 0  ; i--){
        tamiser(tab,i,taille-1);
    }
    
    for(i = taille - 1 ; i >= 1; i--){
        permuter(&tab[i],&tab[0]);
        tamiser(tab,0,i-1);        
    }    
}

void affiche_tab(int *tab, int taille){
    int i = 0;
    fprintf(stdout,"[");
    for(i =0; i< taille-1; i++){
        fprintf(stdout,"%d, ",tab[i]);
    }
    fprintf(stdout,"%d",tab[taille-1]);
    fprintf(stdout,"]\n");
}

void copie_tab(FILE *fichier, int *tab,int *taille){
    int i = 0;
      while( fscanf(fichier,"%d", &tab[i++]) != EOF){
                    *taille = i;
      }
}

